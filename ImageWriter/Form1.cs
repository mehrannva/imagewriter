using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageWriter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        public void GetDatabaseList()
        {
            List<string> list = new List<string>();

            // Open connection to the database
            string conString = textBox1.Text;

            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();

                // Set up a command with the given query and associate
                // this with the current connection.
                using (SqlCommand cmd = new SqlCommand("SELECT name from sys.databases", con))
                {
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                            list.Add(dr[0].ToString());
                        
                    }
                }
            }
            comboBox1.Items.AddRange(list.ToArray());
            comboBox1.SelectedItem = comboBox1.Items[0];
        }

        private void GetTableList()
        {
            string conString = textBox1.Text;
            conString += "Database=" + comboBox1.SelectedItem.ToString();
            comboBox2.Items.Clear();
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                DataTable dt = con.GetSchema("Tables");
                foreach (DataRow item in dt.Rows)
                {
                    var tableName = item["TABLE_NAME"].ToString().Trim();
                    comboBox2.Items.Add(tableName);
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e) => GetTableList();

        private void button1_Click(object sender, EventArgs e)
        {
            var fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
                label1.Text = fbd.SelectedPath;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var files = Directory.GetFiles(@label1.Text, "*.*", SearchOption.AllDirectories).ToList();
            string conString = textBox1.Text;
            conString += "Database=" + comboBox1.SelectedItem.ToString() +";";

            var tmpData = new List<string>();
            string stringCmd = "Select ImageName from " + comboBox2.SelectedItem.ToString();

            using (var conn = new SqlConnection(conString))
            using (var cmd = new SqlCommand(stringCmd, conn))
            {
                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    var dataTable = new DataTable();
                    dataTable.Load(reader);
                    foreach (DataRow item in dataTable.Rows)
                            tmpData.Add(item.ItemArray[0].ToString());
                }
                if (conn.State == ConnectionState.Open)
                    conn.Close();
            }

            var tmpFiles = files.ToList();
            Parallel.ForEach(tmpFiles, item => {
                var fileName = Path.GetFileName(item);
                if (tmpData.Any(x => x == fileName))
                    files.Remove(item);
            });

            MessageBox.Show("تعداد فایل های جدید " + files.Count());

            progressBar1.Maximum = files.Count();
            progressBar1.Step = 1;
            var tmpTableName = comboBox2.SelectedItem.ToString();

            if (files.Count() > 0)
            {
                Parallel.ForEach(files, item =>
                {
                    if (item != null)
                    {
                        var file = File.ReadAllBytes(item);
                        var fileName = Path.GetFileName(item);
                        string stringCommad = "insert into " + tmpTableName + " (ImageName, [Image]) values ('" +
                                                    fileName + "', @Image)";

                        if (file != null && fileName != null)
                        {
                            using (var conn = new SqlConnection(conString))
                            using (var cmd = new SqlCommand(stringCommad, conn) { CommandTimeout = 0/*, CommandType = CommandType.StoredProcedure*/})
                            {
                                cmd.Parameters.AddWithValue("@Image", file);
                                if (conn.State == ConnectionState.Closed)
                                    conn.Open();
                                cmd.ExecuteNonQuery();
                                if (conn.State == ConnectionState.Open)
                                    conn.Close();
                            }
                           // progressBar1.PerformStep();
                        }

                    }
                });

            }
            else
                MessageBox.Show("تصاویر انتخاب شده قبلا ثبت شده است");

            //foreach (var item in files)
            //{
            //    using (var conn = new SqlConnection(conString))
            //    using (var cmd = new SqlCommand("InsertSP", conn)  { CommandType = CommandType.StoredProcedure })
            //    {
            //        var file = File.ReadAllBytes(item);
            //        var fileName = Path.GetFileName(item);

            //        cmd.Parameters.AddWithValue("@name", fileName);
            //        cmd.Parameters.AddWithValue("@file", file);
            //        conn.Open();
            //        cmd.ExecuteNonQuery();
            //    }
            //}
        }

        private void button3_Click(object sender, EventArgs e)
        {
            GetDatabaseList();
            GetTableList();
        }
    }
}
